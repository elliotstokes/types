// @flow

type Action1 = {
  type: 'ACTION1',
  address: string
}

type Action2 = {
  type: 'ACTION2',
  price: number
}

//Type of Action can be an Action1 or an Action2
type Action = Action1 | Action2

type State = {
  address: string,
  price: number
}

const initialState:State = {
  address: 'blah',
  price: 400
}



function reducer(state: State = initialState, action: Action): State {
  switch (action.type) {
    case 'ACTION1':
      //Type infered to be Action1 as we have switched on the type prop
      return {
        ...state,
        address: action.price
      }

      case 'ACTION2':
        //Type infered to be Action2 as we have switched on the type prop
        return {
          ...state,
          price: action.price
        }
      default:
        return state;
  }
}
