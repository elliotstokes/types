//@flow

type Address = {
  postcode: string
}

type Response = {
  name: string,
  address?: Address
}

//Maybe T = Just T | Nothing

/*
Attempting to access a maybe type without first checking it has a value
*/
function doSomethingSpecial(data: Response): string {
  const postcode = data.address.postcode;
  return postcode;
}


/*
Correctly checking a maybe type but then returning nothing on a non-null type
*/
function doSomethingSpecial2(data: Response): string {
  if (!data.address) return;
  const postcode = data.address.postcode;
  return postcode;
}

/*
Correctly checking a maybe type.
*/
function doSomethingSpecial3(data: Response): string {
  const postcode = data.address ? data.address.postcode : 'merp';
  return postcode;
}

/*
Correctly checking a maybe type and returning a maybe type.
*/
function doSomethingSpecial4(data: Response): ?string {
  if (!data.address) return;
  const postcode = data.address.postcode;
  return postcode;
}
