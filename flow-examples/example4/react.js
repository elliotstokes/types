// @flow

import React, { Component } from 'react'
import { NameView } from './nameView'

type Props = {
  appName:string
}

type State = {
  ticks: number
}

/*

1. typing of the Props
2. Typing of the Props being passed into NameView
3. Type checking of the state, including the setState function.

*/

class MyComponent1 extends Component {
  props: Props;
  state: State;
  static defaultProps: Props = {
    appName: 'app name'
  };

  constructor(props: Props) {
    super(props);
    console.log(this.props.foo);

    this.state = {
      ticks: 0
    };

  }

  _buttonPressed() {
    this.setState({
      invalidValue:'wrong'
    });
  }

  render() {
    return (
      <div>
        <span>{this.props.doesntExist}</span>
        <span>{this.state.doesntExist}</span>
        <span>{this.state.ticks}</span>
        <button onClick={this._buttonPressed.bind(this)}>click</button>
        <NameView secondName="stokes"/>
      </div>
    )
  }
}
