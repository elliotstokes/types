// @flow <======Add this to the top of your file to get flow to start type checking

type Response = {
  hello:string,
  world:string,
  enum: 2 | 4 | 6
}


/*
Example of not returning the correct T from the Promise<T>
*/
function addSomeNumbersAsync1(number1: number, number2: number): Promise<Response> {
  return new Promise((res, rej) => {
    setTimeout(()=> res({}), 500);
  });
}

/*
Example of  returning the correct T but the wrong typed parameters.
*/
function addSomeNumbersAsync2(number1: number, number2: number): Promise<Response> {
  return new Promise((res, rej) => {
    setTimeout(()=> res({hello: 44, world: 55, enum: 3}), 500);
  });
}

/*
Example of  returning the correct T from Promise<T>
*/
function addSomeNumbersAsync3(number1: number, number2: number): Promise<Response> {
  return new Promise((res, rej) => {
    setTimeout(()=> res({hello: '44', world: '55', enum: 2}), 500);
  });
}


//Consuming the function correctly
addSomeNumbersAsync3(55, 66).then(res => alert(res.hello));

//Consuming the function correctly and then using a parameter that does not exist
addSomeNumbersAsync3(55, 66).then(res => alert(res.doesnExist));
