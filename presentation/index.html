<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title>Flow</title>

		<link rel="stylesheet" href="css/reveal.css">
		<link rel="stylesheet" href="css/theme/black.css">

		<!-- Theme used for syntax highlighting of code -->
		<link rel="stylesheet" href="lib/css/zenburn.css">

		<!-- Printing and PDF exports -->
		<script>
			var link = document.createElement( 'link' );
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
			document.getElementsByTagName( 'head' )[0].appendChild( link );
		</script>
	</head>
	<body>
		<div class="reveal">
			<div class="slides">
				<section>
					<h3>Typed Programming for JavaScript Engineers</h3>
					<h4>Elliot Stokes &amp; Dave Beaumont</h4>
					<aside class="notes">
						<p>
							Hi everyone, thank you all for coming. Elliot and I are going to
							talk about the static type checkers available to JavaScript developers
							and the advantages they offer for prevention of errors and support
							of refactoring.
						</p>
						<p>
							First we’ll do a recap on the kinds of errors that we’re currently
							seeing in our applications due to type mismatches. From there
							we’ll focus on the type checker that is seeing the most
							adoption in industry, Flow from Facebook, and give a demo of
							its features.
						</p>
					</aside>
				</section>
				<section>
					<h3>JavaScript and Types</h3>
					<section>
						<pre>
							<code data-trim data-noescape>
> {} + {}
"[object Object][object Object]"
							</code>
						</pre>
					</section>
					<section>
						<pre>
							<code data-trim data-noescape>
> {} + []
0
							</code>
						</pre>
					</section>
					<section>
						<pre>
							<code data-trim data-noescape>
> 1 + "1"
"11"
							</code>
						</pre>
					</section>
					<section>
						<pre>
							<code data-trim data-noescape>
> var x = function(y) {
	  if (typeof y === 'object') {
		  console.log('hello', y.z)
	  }
  }
> x(null)
VM500:1 Uncaught TypeError: Cannot read property ‘z’ of null
							</code>
						</pre>
						<aside class="notes">
							<p>
								JavaScript has a lot of foibles that you can read in any clickbait
								"10 things you wouldn't believe about JavaScript" article. While
								some of these results of being weakly typed are a bit weird, we're
								all aware of them and they don't generally cause us too much pain
								in our day-to-day work.
							<p>
							<p>
								Can anyone shout out what's the problem in this last example?
							</p>
						</aside>
					</section>
					<section>
						<h4>¯\_(ツ)_/¯</h4>
						<pre>
							<code data-trim data-noescape>
> typeof null
'object'
							</code>
						</pre>
					</section>
				</section>

				<section>
					<h3>Errors in Real JavaScript Applications</h3>
					<section>
						<h4>HAPI</h4>
						<pre>
							<code data-trim data-noescape>
TypeError: upgrades.map is not a function
  at Function.postBookingUpgrades._transform
							</code>
						</pre>
						<pre>
							<code data-trim data-noescape>
if (!upgrades instanceof Array) upgrades = [ upgrades ];
upgrades.map(x => transformUpgrade(x));
							</code>
						</pre>
						<aside class="notes">
							<p>
								So while those foibles can occassionally trip us up, for the most
								part we remember to work around them. On the other hand, that JavaScript
								variables and parameters are untyped does cause real bugs.
							</p>
							<p>
								This first one is in HAPI and caused a production crash this week.
								The first part is the error and the second part is the code the stacktrace
								referred to.
							</p>
							<p>
								Hands up if you can see what the problem is (pest controllers excluded)?
							</p>
						</aside>
					</section>
					<section>
						<h4>Render</h4>
						<pre>
							<code data-trim data-noescape>
window.onerror: Object doesn't support this action
							</code>
						</pre>
						<aside class="notes">
							<p>
								This second one is in Render and I'm not sure what the cause is,
								but the error message is IE9's variation on `undefined is not a function`.
							</p>
						</aside>
					</section>
					<section>
						<h4>TripApp</h4>
						<pre>
							<code data-trim data-noescape>
Uncaught TypeError: e.replace is not a function
  at D.r.productTypeToHAPIProductType (c0e44907cfa00b937302.tripapp.gz.js:34)
							</code>
						</pre>
						<pre>
							<code data-trim data-noescape>
this._prefetchProducts(...routeParams);
							</code>
						</pre>
						<aside class="notes">
							<p>
								And this one was caused by me in TripApp this week.
							</p>
						</aside>
					</section>
					<section data-background-image="assets/cry.jpg">
						<h4>More TripApp...</h4>
						<pre>
							<code data-trim data-noescape>
TypeError / 'undefined' is not a function
  at <anonymous>:1:25
							</code>
						</pre>
						<aside class="notes">
							<p>
								The previous error was not too hard to debug, but they can be
								more difficult. This is another real bug pulled out of TripApp's
								bugsnag this week.
							</p>
							<p>
								In some cases we see at the point of writing code the possibility
								that a variable could hold the wrong kinds of data and the use
								of null guards at the start of functions as well as library
								features such as lodash’s _.result and _.get.
							</p>
							<p>
								These techniques effectively build up a static type system
								defined in application code. This domain-specific type system
								specifies rules such as “This parameter is non-nullable”, or
								“This object must have a method named ‘presenter’”.
							</p>
							<p>
								That has the disadvantage of the maintenance burden being on our
								engineers and it is easy to miss a case.
							</p>
						</aside>
					</section>
				</section>

				<section>
					<blockquote>
						Very large JavaScript code bases tend to become read-only
					</blockquote>
					<p>- Anders Hejlsberg</p>
					<aside class="notes">
						<p>
							Another problem is that these application specific type systems
							don’t provide any support for refactoring of how
							modules interact with each other. They can't tell you that this object
							produced in one place is the same as that passed down several layers
							and that certain parts are discarded.
						</p>
						<p>
							Are there bits of codebases you work on
							that you would rather not touch for fear of breaking things?
						</p>
					</aside>
				</section>

				<section data-background-image="assets/tag-cloud.png">
					<aside class="notes">
						<p>
							So how can third party tools help us?
						</p>
						<p>Static type checkers aim to improve software development by:</p>
						<ul>
							<li>earlier detection of mistakes before deployment</li>
							<li>better inline documentation for engineers</li>
							<li>support refactoring with a complete view of the application's types</li>
							<li>enable other tooling such as code editors to make more complex changes than find &amp; replace</li>
							<li>
								performance improvements. Unfortunately, as JavaScript engineers we won't benefit
								from these to the same extent as we'll still be targeting the same VM, but Oli alluded to the performance impact of
								typed inter-process communication in how Uber uses thrift. Similarly, the type information that GraphQL
								exposes can be used to improve the performance of client side applications.
						</ul>
						<p>
							With those purported benefits in mind Elliot is going to introduce us to Flow.
						</p>
					</aside>
				</section>

				<!-- Start Flow Section -->

				<section>
					<section>
						<img src="assets/flow-logo.png"/>
					</section>
					<section>
						<h3>History</h3>
						<ul>
							<p>Developed by Facebook and released in 2014</p>
							<p class="fragment">Based in part on the work done on Hack</p>
							<aside class="notes">
								<p>
									Hack - (a static type checker for php)
									Facebook had already tried this for php and made the decision that static typing is a valuable feature that catches issues.
								</p>

								<p>
									Written in oCaml
								</p>
							</aside>
						</li>
					</section>

					<section>
						<h3>Support</h3>
							<p>Works well within the babel/webpack ecosystem</p>
							<p>Great support for React</p>
							<img height="300" src="./assets/react-logo.svg"/>
							<aside class="notes">
								<p>
									a babel-plugin-transform-flow-strip-types plugin is provided to strip out the types at build time
								</p>

								<p>Built in support for JSX. Also works well with react-native</p>

								<p>
									Plugins for atom, emacs and vim to enable code completion.
								</p>

								<p>Provde a cli tool to download library definitions called flow-typed</p>
							</aside>
					</section>

					<section>
						<h3>Library definitions</h3>


						<p>Allows type checking of your code that calls 3rd party apis</p>
						<p>flow-typed: a cli tool to help facilitate</p>
						<img src="assets/flow-typed.png"/>

						<aside class="notes">
							<p>Using flow-typed is easy. Just run flow-typed install within your project</p>
							<p>Uses your package.json file and checks your deps against a central repo and downloads the types into your project</p>
						</aside>
					</section>

					<section>
						<h3>Usage</h3>
						<ul>
							<p>Enabled by default on all react native projects</p>
							<p>Used internally by Facebook</p>
							<p>Used on some major open source projects<p>

							</li>
						</ul>
						<aside class="notes">
							<p>When you create a new react native project the project generated by the cli has flow setup and ready to go</p>
							<p>tell tale sign a project is using flow is a .flowconfig file at the project root</p>
							<p>Used internally within </p>
							<ul>
								<li>React Native</li>
								<li>React</li>
								<li>GraphQL-js</li>
							</ul>
							<p>We are starting to use it on the trip-servie-api that we are working on. Early days though</p>
						</aside>
					</section>
				</section>
				<section>
					<section><h1>Using Flow</h1></section>

					<section>
						<h3>Simplicity</h3>
						<p>Allows incremental static type checking</p>
						<pre><code data-trim data-noescape>
// Just add this when you are ready
// @flow
						</pre></code>

						<p>Infers types if possible</p>
						<pre><code data-trim data-noescape>
//automatically be typed as string.
const stuff = 'cats';
//automatically be typed as number.
const otherstuff = 42;
						</pre></code>

						<aside class="notes">
							<p>Works with all standard javascript syntax and extends the syntax in 2 ways.</p>
						</aside>
					</section>

					<section>
						<h2>Adding a <code>Type</code> keyword</h2>
						<pre><code data-trim data-noescape>
type Animal {
name: string
age: number
isReal: boolean,
legs: 2 | 4 | 6 | 8
}
						</code></pre>
					</section>

					<section>
						<h3>Adding type annotations</h3>
						<pre><code data-trim data-noescape>
function a(var1:string, var2:boolean): string {
	return var2 ? var1 : 'something else';
}
						</code></pre>

						<pre><code data-trim data-noescape>
const vermiciousKnid: Animal = {
	name: 'Vermicious Knid',
	age: 3.14159,
	isReal: false,
	legs: 6
};
						</code></pre>

					</section>
				</section>
				<section>
					<h3>Demo</h3>
					<img src="assets/demo.gif"/>
				</section>

				<section>
					<h3>Summary</h3>
					<ul>
						<li>Self-documenting code</li>
						<li>Developer Experience</li>
						<li>Prevention of Errors</li>
						<br />
						<li>Elliot will cry otherwise.</li>
					</ul>

					<aside class="notes">
						<p>
							So in summary, use of a static type checker allows for much more
							self-documenting code, which is ever important as our team continues
							to grow and people move between pods.
						</p>
						<p>
							The self-documenting code allows for tool support that helps
							developers discover and use APIs correctly without continually having
							to refer github READMEs and other documentation.
						</p>
						<p>
							The techniques we've looked at can prevent whole classes of errors
							and potententially reduce boilerplate guard code and associated
							unit tests.
						</p>
					</aside>
				</section>
			</div>
		</div>

		<script src="lib/js/head.min.js"></script>
		<script src="js/reveal.js"></script>

		<script>
			// More info https://github.com/hakimel/reveal.js#configuration
			Reveal.initialize({
				history: true,

				// More info https://github.com/hakimel/reveal.js#dependencies
				dependencies: [
					{ src: 'plugin/markdown/marked.js' },
					{ src: 'plugin/markdown/markdown.js' },
					{ src: 'plugin/notes/notes.js', async: true },
					{ src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
				]
			});
		</script>
	</body>
</html>
